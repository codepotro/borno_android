/*

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

        Copyright© Jayed Ahsan Saad. All Rights Reserved
*/

package com.codepotro.borno.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

public class IcoText extends TextView {

    private Context context;

    public IcoText(Context context) {
        super(context);
        this.context = context;
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "Bornocode.ttf");
        this.setTypeface(font);
    }

    public IcoText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "Bornocode.ttf");
        this.setTypeface(font);

    }


}