/*

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

        Copyright© Jayed Ahsan Saad. All Rights Reserved
*/

package com.codepotro.borno.Activity;


import android.content.SharedPreferences;

import android.graphics.Color;
import android.os.Bundle;

import android.preference.PreferenceManager;

import android.view.View;

import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import com.codepotro.borno.keyboard.KeyboardTheme;
import com.codepotro.borno.latin.BuildConfig;
import com.codepotro.borno.latin.R;
import com.codepotro.borno.latin.settings.SettingsActivity;
import com.codepotro.borno.latin.settings.SubScreenFragment;
import com.codepotro.borno.latin.settings.ThemeSettingsFragment;
import com.codepotro.borno.latin.settings.TwoStatePreferenceHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.codepotro.borno.latin.common.Constants.ImeOption.BORNO_THEME_KEY;

public class ThemeActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = ThemeActivity.class.getSimpleName();
    public int mSelectedThemeId;


    GridView androidGridView;

    String[] gridViewString = {
            "Borno Light", "Material Light", "Material Light Border", "Material Dark", "Material Dark Border",

    } ;

    int[] gridViewImageId = {
            R.drawable.prev_mlb, R.drawable.prev_ml, R.drawable.prev_mlb, R.drawable.prev_md, R.drawable.prev_mdb,
    };

    int[] ThemeId = {
            1,2,3,4,5
    };












    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);








        CustomGridViewActivity adapterViewAndroid = new CustomGridViewActivity(ThemeActivity.this, gridViewString, gridViewImageId,ThemeId);
        androidGridView=(GridView)findViewById(R.id.grid_view_image_text);
        androidGridView.setAdapter(adapterViewAndroid);
        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {



                TextView slectedtheme =  view.findViewWithTag(ThemeId[+i]) ;
                if (slectedtheme != null) {
                    Toast.makeText(getApplicationContext(), slectedtheme.getText().toString(), Toast.LENGTH_SHORT).show();


                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt(BORNO_THEME_KEY, ThemeId[+i]);
                    editor.commit();


                }









            }
        });




    }





    @Override
    public void onClick(View view) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());






        String tag = String.valueOf(view.getTag());


            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(BORNO_THEME_KEY, tag);
            editor.commit();




    }



}


