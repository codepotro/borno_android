package com.codepotro.borno.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.codepotro.borno.latin.R;

import static com.codepotro.borno.latin.common.Constants.ImeOption.BORNO_THEME_KEY;

public class CustomGridViewActivity extends BaseAdapter {

    private Context mContext;
    private final String[] gridViewString;
    private final int[] gridViewImageId;
    private final int[] ThemeId;

    public CustomGridViewActivity(Context context, String[] gridViewString, int[] gridViewImageId, int[] ThemeId) {
        mContext = context;
        this.gridViewImageId = gridViewImageId;
        this.gridViewString = gridViewString;
        this.ThemeId = ThemeId;
    }

    @Override
    public int getCount() {
        return gridViewString.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int tag = sharedPreferences.getInt(BORNO_THEME_KEY, 1);








        if (convertView == null) {

            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.theme_view, null);
            TextView textViewAndroid = (TextView) gridViewAndroid.findViewById(R.id.android_gridview_text);
            ImageView imageViewAndroid = (ImageView) gridViewAndroid.findViewById(R.id.android_gridview_image);
            textViewAndroid.setText(gridViewString[i]);

            imageViewAndroid.setImageResource(gridViewImageId[i]);




            if  ((ThemeId[i]) == tag)
            {
              //  Toast.makeText(mContext, Integer.toString(ThemeId[i]) + "Matched", Toast.LENGTH_SHORT).show();

                textViewAndroid.setTag(ThemeId[i]);
                textViewAndroid.setTextColor(mContext.getResources().getColor(R.color.gesture_trail_color_lxx_light));

            }
            else
            {
                //   Toast.makeText(mContext, Integer.toString(gridViewImageTag[i]) + "UnMatched", Toast.LENGTH_SHORT).show();

                textViewAndroid.setTag(ThemeId[i]);
                textViewAndroid.setTextColor(mContext.getResources().getColor(R.color.key_text_color_lxx_light));

            }







        } else {
            gridViewAndroid = (View) convertView;
        }

        return gridViewAndroid;
    }
}