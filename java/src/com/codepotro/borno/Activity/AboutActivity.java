/*

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

        Copyright© Jayed Ahsan Saad. All Rights Reserved
*/

package com.codepotro.borno.Activity;

import android.app.AlertDialog;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.codepotro.borno.latin.BuildConfig;
import com.codepotro.borno.latin.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AboutActivity extends AppCompatActivity {

    private static final String TAG = AboutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        AssetManager assetManager = getAssets();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;

        getSupportActionBar().setHomeButtonEnabled(true);



        TextView tv=(TextView)findViewById(R.id.textCred);

        try {

            StringBuilder returnString = new StringBuilder();
            fIn = assetManager.open("creds.txt");
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);

            String line;
            while ((line = input.readLine()) != null) {
                returnString.append(line);
                returnString.append("\n");
            }

            String data = returnString.toString();

            tv.setText(data);

        } catch (IOException e) {

            Log.e(TAG, "Failed to read third party licences");

        } finally {


            try {
                if(fIn != null) fIn.close();
            } catch (IOException ignore) { }

            try {
                if(isr != null) isr.close();
            } catch (IOException ignore) { }

            try {
                if(input != null) input.close();
            } catch (IOException ignore) { }

        }
tv.setMovementMethod(new ScrollingMovementMethod());
    }


    public void onClickLICS(View v){

        AssetManager assetManager = getAssets();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;

        try {

            StringBuilder returnString = new StringBuilder();
            fIn = assetManager.open("third_party_licenses.txt");
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);

            String line;
            while ((line = input.readLine()) != null) {
                returnString.append(line);
                returnString.append("\n");
            }

            String data = returnString.toString();

            new AlertDialog.Builder(this)
                    .setTitle("Third Party Licenses")
                    .setMessage(data)
                    .setNeutralButton("OK", null)
                    .show();

        } catch (IOException e) {

            Log.e(TAG, "Failed to read third party licences");

        } finally {


            try {
                if(fIn != null) fIn.close();
            } catch (IOException ignore) { }

            try {
                if(isr != null) isr.close();
            } catch (IOException ignore) { }

            try {
                if(input != null) input.close();
            } catch (IOException ignore) { }

        }

    }



    public void onClickContri(View v){

        AssetManager assetManager = getAssets();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;

        try {

            StringBuilder returnString = new StringBuilder();
            fIn = assetManager.open("contributors.txt");
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);

            String line;
            while ((line = input.readLine()) != null) {
                returnString.append(line);
                returnString.append("\n");
            }

            String data = returnString.toString();

            new AlertDialog.Builder(this)
                    .setTitle("Contributors")
                    .setMessage(data)
                    .setNeutralButton("OK", null)
                    .show();

        } catch (IOException e) {

            Log.e(TAG, "Failed to read third party licences");

        } finally {


            try {
                if(fIn != null) fIn.close();
            } catch (IOException ignore) { }

            try {
                if(isr != null) isr.close();
            } catch (IOException ignore) { }

            try {
                if(input != null) input.close();
            } catch (IOException ignore) { }

        }

    }

}
