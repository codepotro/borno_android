/*

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

        Copyright© Jayed Ahsan Saad. All Rights Reserved
*/



package com.codepotro.borno;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.codepotro.borno.latin.BuildConfig;
import com.codepotro.borno.latin.R;
import com.codepotro.borno.latin.settings.SettingsActivity;
import com.codepotro.borno.Activity.AboutActivity;

public class StartUpActivity extends AppCompatActivity {

    private static final String TAG = StartUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.borno_startup);
        getSupportActionBar().setHomeButtonEnabled(true);


        TextView AppInstruct = findViewById(R.id.inslab);



    }



    public void onClickAbout(View v){



        try {

            Intent myIntent = new Intent(StartUpActivity.this, AboutActivity.class);
            startActivity(myIntent);
            finish();

        } catch (Exception e) {

            Log.e(TAG, "Failed to open About Section");

        } finally {




        }

    }

    public void onClickSettings(View v){

        try {

            Intent myIntent = new Intent(StartUpActivity.this, SettingsActivity.class);
            startActivity(myIntent);
            finish();

        } catch (Exception e) {

            Log.e(TAG, "Failed to open About Section");

        } finally {




        }

    }

    public void onClickCodepotro(View v){
        try {

            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://codepotro.com"));
            startActivity(myIntent);
            finish();

        } catch (Exception e) {

            Log.e(TAG, "Failed to open About Section");

        } finally {




        }

    }

    public void onClickPCApp(View v){
        try {

            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://codepotro.com/borno"));
            startActivity(myIntent);
            finish();

        } catch (Exception e) {

            Log.e(TAG, "Failed to open About Section");

        } finally {




        }

    }
}
