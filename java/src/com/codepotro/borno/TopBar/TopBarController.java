/*

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

        Copyright© Jayed Ahsan Saad. All Rights Reserved
*/

package com.codepotro.borno.TopBar;

import android.view.View;
import android.widget.LinearLayout;
import com.codepotro.borno.latin.R;
import com.codepotro.borno.latin.suggestions.SuggestionStripView;
import com.codepotro.borno.TopBar.Row.RowView;




public class TopBarController {
    final Runnable hideSuggestionAfter;
    private final LinearLayout holderLayout;
    private final RowView mActionRowView;
    private final View mSuggestionsStripHackyContainer;
    private final SuggestionStripView mSuggestionsStripView;


    public int getHeight() {
        return holderLayout.getHeight();
    }

    class Hider implements Runnable {
        Hider() {
        }
        public void run() {
            mActionRowView.setVisibility(View.VISIBLE);
            mSuggestionsStripHackyContainer.setVisibility(View.GONE);
        }
    }

    public void updateBarVisibility() {
        mActionRowView.setVisibility(View.VISIBLE);
        mSuggestionsStripHackyContainer.setVisibility(View.GONE);
    }



    public TopBarController(View parent){
        hideSuggestionAfter = new Hider();
        holderLayout = (LinearLayout) parent.findViewById(R.id.keyboard_top_area);
        mActionRowView = (RowView) parent.findViewById(R.id.action_row);
        mSuggestionsStripView = (SuggestionStripView) parent.findViewById(R.id.suggestion_strip_view);
        mSuggestionsStripHackyContainer = parent.findViewById(R.id.suggestion_strip_ex_container);
        mSuggestionsStripHackyContainer.setVisibility(View.GONE);

    }

    public void showSuggestions() {
        if (this.mSuggestionsStripView.getVisibility() == View.VISIBLE) {

            mActionRowView.removeCallbacks(this.hideSuggestionAfter);
            mActionRowView.setVisibility(View.GONE);
            this.mSuggestionsStripHackyContainer.setVisibility(View.VISIBLE);
            mActionRowView.postDelayed(this.hideSuggestionAfter, 10000);
        }
    }
}
