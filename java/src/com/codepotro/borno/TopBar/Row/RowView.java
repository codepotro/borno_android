/*

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

        Copyright© Jayed Ahsan Saad. All Rights Reserved
*/

package com.codepotro.borno.TopBar.Row;

import android.content.Context;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codepotro.borno.latin.AudioAndHapticFeedbackManager;
import com.codepotro.borno.latin.R;
import com.codepotro.borno.ui.IcoText;

import java.util.ArrayList;
import java.util.HashMap;





public class RowView extends ViewPager implements  View.OnTouchListener {



    private RowAdapter adapter;

    private String[] layoutToShow;
    private Listener mListener;
    private HashMap<String, LinearLayout> layouts;


    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

   

    /* onSelectAll */
    class onSelectAll implements OnClickListener {
        onSelectAll() {
        }

        public void onClick(View v) {
            mListener.onSelectAll();
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
        }
    }

    /* onCut */
    class onCut implements OnClickListener {
        onCut() {
        }

        public void onClick(View v) {
            mListener.onCut();
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
        }
    }

    /* onSett */
    class onSett implements OnClickListener {
        onSett() {
        }

        public void onClick(View v) {
            mListener.onSett();
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
        }
    }

    /* onThemer */
    class onThemer implements OnClickListener {
        onThemer() {
        }

        public void onClick(View v) {
            mListener.onThemer();
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
        }
    }

    /* onCopy */
    class onCopy implements OnClickListener {
        onCopy() {
        }

        public void onClick(View v) {
            mListener.onCopy();
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
        }
    }
    /* onEmo */
    class onEmo implements OnClickListener {
        onEmo() {
        }

        public void onClick(View v) {
            mListener.onEmo();
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
        }
    }


    public interface Listener {
        void onEmo();

        void onCopy();

        void onCut();
        void onSett();
        void onThemer();

        void onEmojiClicked(String str, boolean z);


        void onPaste();

        void onSelectAll();



    }

    private class RowAdapter extends PagerAdapter {
        private ArrayList<View> views;

        private RowAdapter() {
            this.views = new ArrayList();
        }

        @Override
        public int getItemPosition(Object object) {
            int index = this.views.indexOf(object);
            if (index == -1) {
                return -2;
            }
            return index;
        }

        public Object instantiateItem(ViewGroup container, int position) {
              View v = layouts.get(RowView.this.layoutToShow[position % RowView.this.layoutToShow.length]);
            this.views.add(v);
            container.addView(v);
            return v;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        public int getCount() {
            return RowView.this.layoutToShow.length;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }


    }





    public RowView(Context context) {
        super(context);

        init();
    }

    public RowView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }


    protected void onPageScrolled(int position, float offset, int offsetPixels) {
        super.onPageScrolled(position, offset, offsetPixels);
    }



    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

    }



    private void init() {

        layoutToShow = RowParser.DEFAULT_LAYOUTS.split("\\s*,\\s*");
        adapter = new RowAdapter();
        setAdapter(adapter);

        setupLayouts();
    }

    private void setupLayouts(){
        layouts = new HashMap<>();
        layouts.put(RowParser.CLIP_ID, addButtons());
    }

    private View addEmptyView() {
        return new LinearLayout(getContext());
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public View createViewFromID(String identifier) {

        if (identifier.equals(RowParser.CLIP_ID))
            return addButtons();

        return addEmptyView();
    }

    private LinearLayout addButtons() {
        LinearLayout layout = (LinearLayout) View.inflate(getContext(), R.layout.expanded_bar, null);

        IcoText emoji = (IcoText) layout.findViewById(R.id.emoji);
        emoji.setSoundEffectsEnabled(false);
        emoji.setOnClickListener(new onEmo());
        emoji.setBackgroundResource(R.drawable.action_row_bg);


        IcoText sett = (IcoText) layout.findViewById(R.id.sett);
        sett.setSoundEffectsEnabled(false);
        sett.setOnClickListener(new onSett());
        sett.setBackgroundResource(R.drawable.action_row_bg);

        IcoText themer = (IcoText) layout.findViewById(R.id.themer);
        themer.setSoundEffectsEnabled(false);
        themer.setOnClickListener(new onThemer());
        themer.setBackgroundResource(R.drawable.action_row_bg);

        IcoText selectAll = (IcoText) layout.findViewById(R.id.select);
        selectAll.setSoundEffectsEnabled(false);
        selectAll.setOnClickListener(new onSelectAll());
        selectAll.setBackgroundResource(R.drawable.action_row_bg);

        IcoText cut = (IcoText) layout.findViewById(R.id.cut);
        cut.setSoundEffectsEnabled(false);
        cut.setOnClickListener(new onCut());
        cut.setBackgroundResource(R.drawable.action_row_bg);

        IcoText copy = (IcoText) layout.findViewById(R.id.copy);
        copy.setSoundEffectsEnabled(false);
        copy.setOnClickListener(new onCopy());
        copy.setBackgroundResource(R.drawable.action_row_bg);

        IcoText paste = (IcoText) layout.findViewById(R.id.paste);
        paste.setSoundEffectsEnabled(false);
        paste.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mListener.onPaste();
                AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
            }
        });
        paste.setBackgroundResource(R.drawable.action_row_bg);
        return layout;
    }





    class AnonymousClass11 implements OnClickListener {
        final TextView val$view;

        AnonymousClass11(TextView  emojiconTextView) {
            this.val$view = emojiconTextView;
        }

        public void onClick(View tview) {
            AudioAndHapticFeedbackManager.getInstance().performHapticAndAudioFeedback(-15, RowView.this);
            mListener.onEmojiClicked(this.val$view.getText().toString(), false);
        }
    }






}
